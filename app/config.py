import os
from distutils.util import strtobool


def env_strtobool(val):
    if val in ["true", "'true'"]:
        return True
    return False


class BaseConfig(object):
    APP_BASE_URL = os.environ["APP_BASE_URL"]
    APP_LOGLEVEL = os.environ["APP_LOGLEVEL"]

    DB_NAME = os.environ["DB_NAME"]
    DB_USER = os.environ["DB_USER"]
    DB_PASS = os.environ["DB_PASS"]
    DB_SERVICE = os.environ["DB_SERVICE"]
    DB_PORT = os.environ["DB_PORT"]

    DEBUG = os.environ["DEBUG"]

    DEFAULT_ADMIN_USER = os.environ["DEFAULT_ADMIN_USER"]
    DEFAULT_ADMIN_PASSWORD = os.environ["DEFAULT_ADMIN_PASSWORD"]

    LOG_TO = os.environ["LOG_TO"].split(",")

    GRAYLOG_HOST = os.environ["GRAYLOG_HOST"]
    GRAYLOG_PORT = int(os.environ["GRAYLOG_PORT"])
    GRAYLOG_APPNAME = "tracker"
    GRAYLOG_APPENV = os.environ["DEPLOY_ENV"]

    SECRET_KEY = os.environ["SECRET_KEY"]
    SECURITY_PASSWORD_HASH = os.environ["SECURITY_PASSWORD_HASH"]
    SECURITY_PASSWORD_SALT = os.environ["SECURITY_PASSWORD_SALT"]
    SECURITY_RECOVERABLE = os.environ["SECURITY_RECOVERABLE"]
    SECURITY_CHANGEABLE = os.environ["SECURITY_CHANGEABLE"]

    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DefaultConfig(BaseConfig):

    # Statement for enabling the development environment
    # DEBUG = True
    pass
